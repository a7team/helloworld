package com.academy7.HelloWorld.main;

public class User {
	
	// initialize local variables
	private String lastName = null;
	private String firstName = null;
	
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName.toUpperCase();
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName.toLowerCase();
		if (this.firstName.length() > 0) {
			String atFirst = "" + this.firstName.charAt(0);
			if (this.firstName.length() > 1) {
				this.firstName = atFirst.toUpperCase() + this.firstName.substring(1);
			}
			else {
				this.firstName = atFirst.toUpperCase();
			}
		}
		
	}//test
	public int test;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getFirstName() + " " + getLastName();
	}
	

}
