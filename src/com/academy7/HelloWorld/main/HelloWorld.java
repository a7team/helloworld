package com.academy7.HelloWorld.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HelloWorld {

	// ceci est un commentaire

	
	private static String askSthing(String askedThing) throws IOException {
		
		// initialize input buffer
		InputStreamReader in = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		
		
		// answer from user input
		String answer = null;
		
		while( (answer == null) || (answer.length() == 0 ) ) {
			// ask
			System.out.println("What's " + askedThing + " ?");
			// read
			answer = br.readLine();
			if ( (answer == null) || (answer.length() == 0 ) ) {
				System.out.println("Please type " + askedThing + " then press Enter.");
			}
		}
		
		return answer;
		
	}
	
	public static void main(String[] args) {
		
		User user = new User();
		
		// output an echo
		System.out.println("Hello world !");
		
		try {
			
			user.setLastName(askSthing("your last name"));
			
			// echo last name
			System.out.println("Hello " + user.getLastName());
			
			user.setFirstName(askSthing("your first name"));
			
			// echo full name
			System.out.println("Hello " + user);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		// echo EOT
		System.out.println("Thank you very much, I hope to see you soon.");
		
		
	}
	
}
